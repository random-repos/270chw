#pragma once
#include "QuClutils.h"
#include "algebra.h"
#include "constants.h"

QuStupidPointer<SPARSE_MATRIX<prec> > getA1(int n)
{
    assert( n != 0 );
    float m1odx = -(n-1);
    float 2odx = (n-1)*2;
    QuStupidPointer<SPARSE_MATRIX<prec> > r(new SPARSE_MATRIX<prec>(n,n));
    for(int i = 0; i < n; i++)
    {
        if(i != 0)
            r(i,i-1) = m1odx;
        r(i,i) = 2odx;
        if(i != n-1)
            r(i,i+1) = m1odx;
    }
    return r;
}

QuStupidPointer<VECTOR<prec> > getb1(int n, prec a, prec b)
{
    QuStupidPointer<VECTOR<prec> > r(new VECTOR<prec>(n));

}


QuStupidPointer<SPARSE_MATRIX<prec> > getA2(int n)
{
    assert( n != 0 );
    float odx = n-1;
    float m1odx = -(n-1);
    float 2odx = (n-1)*2;
    QuStupidPointer<SPARSE_MATRIX<prec> > r(new SPARSE_MATRIX<prec>(n,n));
    for(int i = 1; i < n-2; i++)
    {
        if(i != 0)
            r(i,i-1) = m1odx;
        r(i,i) = 2odx;
        if(i != n-1)
            r(i,i+1) = m1odx;
    }
    r(0,0) = r(n-2,n-2) = odx;
    r(0,1) = r(n-2,n-3) = m1odx;
    r(0,n-1) = 1;

    r(n-1,0) = 1;
    return r;
}
