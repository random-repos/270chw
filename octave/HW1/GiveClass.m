1;

function g = GivensClass(i,j,x,y);
    mag = sqrt(x*x + y*y);
    g.c = x/mag;
    g.s = -y/mag;
    g.index1 = i;
    g.index2 = j;
endfunction
function r = GivensClassApply(g,v)

    c = g.c;
    s = g.s;
    i = g.index1;
    j = g.index2;

    r = v;
    r(i) = v(i) * c + v(j) * s;
    r(j) = v(i) * (-s) + v(j) * c;
endfunction
function r = GivensClassApplyTrans(g,v)
    
    c = g.c;
    s = g.s;
    i = g.index1;
    j = g.index2;

    r = v;
    r(i) = v(i) * c + v(j) * (-s);
    r(j) = v(i) * s + v(j) * c;
endfunction
