1;




utils;
function [x,res] = solveCG(N,maxIt)
    if maxIt > N-1
        maxIt = N-1;
    endif
    
    A = symPosDefMat(N);
    b = symPosDefRhs(N);
    M = N-1;

    res = zeros(1,maxIt);

    #initial guess and residual at zeroth iteration
    x = zeros(M,1);
    r = b;

    rho = zeros(M,1);
    rho(1) = normsquared(r);

    #our initial search direction
    p = r;

    for i = 1:maxIt
        i
        if(i > 1)
            betak = rho(i)/rho(i-1);
            p = r + betak*p;
        endif

        omega = A*p;
        alphak = rho(i)/dot(p,omega);
        x = x + alphak*p;
        r = r - alphak*omega;
        res(i) = norm(r);
        if i < M
            rho(i+1) = normsquared(r);
        endif
    endfor
endfunction
