1;

solutions;
problems;
utils;
jacobigs;
conjGrad;

function plotDiscretization(varargin)
    figure;
    for i = 1:length(varargin)
        #plot(1:length(varargin{i}),varargin{i});
        plot(varargin{i});
        hold on;
    endfor
    hold off;
endfunction

function plotJacobi(N,ITER)
    ap = solveJacobi(N,ITER);
    sol = trueSolutionNm1(N);
    disp("linf norm of error is "), disp(supNorm(ap-sol));
    plotDiscretization(ap,sol);
endfunction

function plotGS(N,ITER)
    ap = solveGS(N,ITER);
    sol = trueSolutionNm1(N);
    disp("linf norm of error is "), disp(supNorm(ap-sol));
    plotDiscretization(ap,sol);
endfunction


function plotGSNonSym(N,ITER)
    ap = solveGSNonSym(N,ITER);
    sol = trueSolutionNm1(N);
    disp("linf norm of error is "), disp(supNorm(ap-sol));
    plotDiscretization(ap,sol);
endfunction


function plotCG(N,ITER)
    ap = solveCG(N,ITER);
    sol = trueSolutionNm1(N);
    disp("final error "), disp(supNorm(ap-sol));
    plotDiscretization(ap, sol);
endfunction

function plotDefaultNm1(N)
    A = symPosDefMat(N);
    b = symPosDefRhs(N);
    M = N-1
    sol = trueSolutionNm1(N);
    ans = inverse(A) * b;
    plotDiscretization(sol,ans);
endfunction
