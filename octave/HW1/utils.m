1;

function r = supNorm(x)
    r = 0;
    for i = 1:length(x)
        if abs(x(i)) > r
            r = abs(x(i));
        endif
    endfor
endfunction

function r = normsquared(v)
    r = 0;
    for i = 1:length(v)
        r += v(i)*v(i);
    endfor
endfunction

