1;

function r = forcing(x)
    r = 4*pi*pi*sin(2*pi*x);
endfunction

function r = forcingDis(i,N)
    r = forcing((i-1)/(N-1));
endfunction

function r = forcingIvZero()
    r = 0;
endfunction

function r = forcingIvOne()
    r = 0;
endfunction;

function r = forcingBarDis(i,N)
    #remember indexing is 1:N but here we want 0:N-1
    xi = (i-1)/(N-1);
    xip1 = i/(N-1);
    r = 2*pi*(cos(2*pi*xi)-cos(2*pi*xip1)) * (N-1);
endfunction

function r = symPosDefMat(N)
    m = N-1;
    r = sparse(m,m);
    for i = 1:m
        r(i,i) = 2;
    endfor
    for i = 2:m
        r(i-1,i) = -1;
        r(i,i-1) = -1;
    endfor
    #r(m,m) = 1;
    dx = 1/m;
    r = r / dx;
endfunction

function r = symPosDefRhs(N)
    r = zeros(N-1,1);
    for i = 1:N-2
        r(i) = (forcingBarDis(i,N)+forcingBarDis(i+1,N))/2 /(N-1) + forcingIvZero() * (N-1);
    endfor
    r(N-1) = forcingBarDis(N-1,N)/2/(N-1) + forcingIvOne();
endfunction

function r = nonSymMat(N)
    m = N-1;
    r = sparse(m,m);
    for i = 1:m
        r(i,i) = 2;
    endfor
    for i = 2:m
        r(i-1,i) = -1;
        r(i,i-1) = -1;
    endfor
    dx = 1/m;
    r(m,m) = dx;
    r(m,m-1) = -dx;
    r = r / (dx*dx);
endfunction

function r = nonSymRhs(N)
    M = N-1;
    r = zeros(M,1);
    for i = 1:M-1
        r(i) = forcingDis(i,N);
    endfor
    dx = 1/M;
    r(1) += forcingIvZero()/(dx*dx);
    r(M) = forcingIvOne();
endfunction
