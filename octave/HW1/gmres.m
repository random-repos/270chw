1;

GiveClass;

function [x,r] = GMRES(A,b,N,maxIt)
    #allocate R (NxN-1)? (use submatrix operator to get out what you need
    #allocate givens rotations (use cells)
    #allocate 

    m = min(N,maxIt);
    
    r = zeros(1,m-1);

    Q = zeros(N,N); #our qk vectors
    Q(:,1) = b/norm(b);
    R = zeros(m,m-1); #or was in m,m-1
    G = cell(m,1); #our m givens rotations
    bhat = zeros(m,1);
    bhat(1) = norm(b);

    for k = 2:m
        #compute our h_lk scalars

        #MGS
        #compute Aq_km1
        Aqkm1 = A * Q(:,k-1);

        vk = Aqkm1;

        #compute hlkm1s and vk = Aqkm1 - sum(hlkm1*ql)
        for l = 1:k-1
            H(l) = dot(Q(:,l),Aqkm1);
            vk -= H(l) * Q(:,l);
        endfor

        #compute and qk and hkkm1
        H(k) = norm(vk);
        Q(:,k) = vk/H(k);

        #i.e. now we have AQkm1 in terms of Q and H

        #time to do givens QR


        #apply k-1 givens to H
        for l = 2:k-1
            H = GivensClassApplyTrans(G{l},H);
        endfor

        #compute G(k)
        G{k} = GivensClass(k-1,k,H(k-1),H(k));
        #apply to bhat
        bhat = GivensClassApplyTrans(G{k},bhat);
        H = GivensClassApplyTrans(G{k},H);

        #append H to R
        #note H at kth iteration is the k-1th collumn of R
        R(1:k,k-1) = H;

        #cheater backsolve it
        lambdak = inverse(R(1:k-1,1:k-1))*bhat(1:k-1); #note last entry of b is of course whats left in least squares

        #was our solution good enough?
        x = Q(:,1:k-1)*lambdak;

        #r(k-1) = abs(bhat(k));
        r(k-1) = norm(A*x - b);
    endfor
endfunction

problems;
function [x,r] = solveGMRESNonSym(N,maxIt)
    A = nonSymMat(N);
    b = nonSymRhs(N);
    [x,r] = GMRES(A,b,N-1,maxIt);
endfunction

function [x,r] = solveGMRESSymPosDef(N,maxIt)
    A = symPosDefMat(N);
    b = symPosDefRhs(N);
    [x,r] = GMRES(A,b,N-1,maxIt);
endfunction
