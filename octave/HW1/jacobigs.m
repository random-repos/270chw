1;

problems;
function [x,res] = solveJacobi(N,maxit)
    A = symPosDefMat(N);
    b = symPosDefRhs(N);
    M = N-1

    res = zeros(1,maxit);

    x = zeros(M,1);
    for k = 1:maxit
        r = b - A*x;
        res(k) = norm(r);
        for i = 1:M
            x(i) += r(i)/A(i,i);
        endfor
    endfor
endfunction

function [x,res] = gaussSiedel(A,b,N,maxit)
    M = N-1;
    x = zeros(M,1);
    res = zeros(1,maxit);
    for k = 1:maxit
        res(k) = norm(b - A*x);
        for i = 1:M
            x(i) += (b(i)-A(i,:) * x)/A(i,i);
        endfor
    endfor
endfunction

function [x,r] = solveGS(N,maxit)
    A = symPosDefMat(N);
    b = symPosDefRhs(N);
    [x,r] = gaussSiedel(A,b,N,maxit)
endfunction

function [x,r] = solveGSNonSym(N,maxit)
    A = nonSymMat(N);
    b = nonSymRhs(N);
    [x,r] = gaussSiedel(A,b,N,maxit);
endfunction
