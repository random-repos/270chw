1;


function x = trueSolutionNm1(N)
    x = zeros(N-1,1);
    for i = 1:N-1
        x(i) = sin(2*pi*(i-1)/(N-1));
    endfor
endfunction

utils;

function [x,y] = plotError(fcn,maxit)
    points = x = [65,129,257,513]
    for i = 1:4
        true = trueSolutionNm1(points(i));
        approx = feval(fcn,points(i),maxit); 
        x(i) = supNorm(true-approx);
    endfor
    y = log(points);
endfunction

function plotResiduals(fcn,maxit,toplabel)
    points = [65,129,257,513];
    for i = 1:4
        [x,r] = feval(fcn,points(i),maxit);
        plot(r);
        xlabel("iteration");
        ylabel("residual");
        title(strcat(toplabel," ",int2str(points(i))));
        print(strcat(fcn,int2str(points(i)),".png"));
    endfor
endfunction
