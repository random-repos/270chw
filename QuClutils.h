#pragma once
#include <map>
#include <math.h>
#include <set>
#include <list>
#include <utility>


class QuStupidPointerHelper
{
public:
	static std::map<void *, int> mSPMap;
	static int add(void * p)
	{
		std::map<void *, int>::iterator it = mSPMap.find(p);
		if(it == mSPMap.end())
		{
			(mSPMap[p] = 0);
			it = mSPMap.find(p);
		}
		(it->second) += 1;
		return it->second;
	}
	static int sub(void * p)
	{
		return (mSPMap[p] -= 1);
	}
	static void remove(void * p)
	{
		mSPMap.erase(p);
	}
	static int getReferenceCount(void * p)
	{
		return mSPMap[p];
	}
};

template<typename T>
class QuStupidPointer
{
	T * mPt;
	int mN;
public:
	//these get called explicitly
	T * rawPointer() const { return mPt; }

	int size() const { return mN; }

	bool isArray() const { return mN > 1; } 

	T * operator->() const { return mPt; }

	T & getReference() const { return *mPt; }

	template<typename S>
	S & getReference() const { return *((S *)mPt); }

	void setNull() { set(NULL,mN); }

	bool isNull() const { return mPt == NULL; }

	T & operator[](const int & i) const { quAssert(mN == -1 || i < mN); return mPt[i]; }

	//does an implicity upcast, so this is safe
	template<typename S>
	QuStupidPointer<S> safeCast() const { return QuStupidPointer<S>(mPt,mN); }

	int getReferenceCount() const
	{ return QuStupidPointerHelper::getReferenceCount(mPt); }

	//do QuStupidPointer(new T(...)) or QuStupidPointer(new T[N],N)
	QuStupidPointer(T * p = NULL, int n = 1):mPt(NULL)
	{
		set(p,n);
	}
	~QuStupidPointer()
	{
		setNull();
	}
	QuStupidPointer & operator=(const QuStupidPointer & o)
	{
		set(o.mPt,o.mN);
		return *this;
	}
	QuStupidPointer(const QuStupidPointer & o):mPt(NULL)
	{
		set(o.mPt,o.mN);
	}
	bool operator==(const QuStupidPointer & o) const
	{
		return mPt == o.mPt;
	}
	//this is just for using in STL containers
	bool operator<(const QuStupidPointer & o) const
	{
		return mPt < o.mPt;
	}

	QuStupidPointer & operator=(T * o)
	{
		//don't do this
		quAssert(false);
		set(o,1);
		return *this;
	}
	bool operator==(const T * o) const
	{
		//don't do this
		quAssert(false); 
		return mPt == o;
	}
	
private:
	//note, these are pass by value thus we are safe from doing self assignment
	void set(T * p, int n)
	{
		//delete old pointer
		if(!isNull())
		{
			if(0 == QuStupidPointerHelper::sub(mPt))
			{
				QuStupidPointerHelper::remove(mPt);
				if(mN == 1)
					delete mPt;
				else
					delete [] mPt;
			}
		}

		mPt = p;
		mN = n;
		if(mPt != NULL)
			QuStupidPointerHelper::add(mPt);
	}
};

